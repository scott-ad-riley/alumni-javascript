function makeRequest(url) {
  return new Promise(function (resolve, reject) {
    const request = new XMLHttpRequest()
    request.open('GET', url)

    request.onload = function () {
      if (this.status !== 200) {
        reject("Something went wrong")
      } else {
        const jsonString = this.responseText
        const data = JSON.parse(jsonString)
        resolve(data)
      }
    }

    request.send()
  })
}

makeRequest('https://restcountries.eu/re...')
  .then(function (parsedJSON) {
    return new Promise
  })
  .catch(function (string) {
    string // "something went wrong"
  })

fetch('https://restcountries...')
  .catch(failure => console.log(failure))
  .then(response => response.json())
  .then(data => console.log(data))
  .catch(failure => console.log(failure))

Promise.all([fetch('some_url'), fetch('another_url')])
  .then(arrayOfResults => arrayOfResults)

// Combining it all together

fetch('https://launchlibrary.net/1.1/launch')
  .then(response => response.json())
  .then(launchList => collectLaunchDetails(launchList))
  .then(launchListWithDetails => doUIStuff(launchListWithDetails))


function collectLaunchDetails(launchList) {
  if (window.savedLaunches) { // if it's run already/saved
    return Promise.resolve(window.savedLaunches)
  } else {
    const launches = Promise.all(
        launchList.map(launch => fetch('https://launchlibrary.net/1.1/launch' + launch))
      )
    window.savedLaunches = launches // store for next time
    return launches
  }
}
