# Promises

## WHY
Useful for:
* writing async code that reads more like synchronous code
* abstract/hide away behaviour that could be async or synchronous

## WHAT

```javascript
  // let's implement our own proimse
  function makeRequest(url) {
    return new Promise((resolve, reject) => {
        const request = new XMLHttpRequest();
        request.open('GET', url);

        request.onload = function () {
            if (this.status !== 200) {
                reject(this);
            } else {
              const jsonString = this.responseText;
              const data = JSON.parse(jsonString);
              resolve(data);
            }
        }
        request.send();
    });
  }

  // and then use it like so:
  makeRequest('https://restcountries.eu/rest/v1/all')
    .then(data => {
        console.log(data);
    })
    .catch(response => {
        console.error('Something went wrong! Response status:', response.status);
    });

  // in this case we're
  fetch('https://restcountries.eu/rest/v1/all')
    .then(response => response.json())
    .then(data => console.log)
```

Additional things to note:

* Try to make sure you don't use anonymous functions in your `then` and `catch` blocks - it makes debugging much more difficult (as well as testing)

* You can actually wrap up both async and synchronous behaviour inside a promise and leave the caller/origin unaware of the implementation, see below;

```javascript
  fetch('https://launchlibrary.net/1.1/launch')
    .then(response => response.json())
    .then(collectLaunchDetails)
    .then(europeanIds => europeanIds.each(doUIStuff))

  function collectLaunchDetails(launchList) {
    if (window.cachedLaunches) { // i.e. a saved version
      return Promise.resolve(window.cachedLaunches)
    } else {
      return Promise.all(
        launchList.map(launch => fetch(`https://launchlibrary.net/1.1/launch/${launch.id}`))
      )
    }
  }
```
