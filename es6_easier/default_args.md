# [NAME]

## WHY
Useful for:
* concisely setting defaults for function parameters

## WHAT

Setting defaults parameters is nothing you couldn't do before es6.
It just looked pretty ugly and verbose in es5.

```javascript
var Bear = function (name, age, weight) {
  this.name = name
  if (age === undefined) {
    this.age = 0
  }
  else {
    this.age = age
  }
  // Or maybe you'd use this shortcut, still not great:
  this.weight = weight || 0.0
}
var babyBear = new Bear('Yogi')
babyBear.age // 0
babyBear.weight // 0.0
```

The same thing can be achieved much more concisely, right in your parameter list.

```javascript
var Bear = function (name, age = 0, weight = 0.0) {
  this.name = name
}
var babyBear = new Bear('Yogi')
babyBear.age // 0
babyBear.weight // 0.0
```

Much better.
