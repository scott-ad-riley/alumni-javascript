# Arrow Functions

## WHY
Useful for:
* Avoiding .bind(this) ugliness all over the place when passing callbacks around
* Nice elegant looking .maps / .filters etc. using implicit return

## WHAT

### Lexical binding of `this`


```javascript
var Timer = function () {
  this.minutes = 0
  setInterval(function () {
    this.minutes++
  }, 60000)
}
```

This won't work. The callback we pass to setInterval has its `this` set to the
`global` / `window` object, not our Timer.

In es5 we solve this using the `.bind` method on the function prototype.

But arrow functions "lexically bind `this`" - The value of `this` is set at to
the value of `this` where the arrow function was defined. Whereas traditional
functions set `this` to the value of `this` where they are invoked.

This means that if we use an arrow function in our Timer constructor, we don't
have to call `.bind(this)`, the arrow function itself will ensure that `this` in
the inner function is the same as `this` in the outer function.

### Implicit return

This is most useful when we're using functional programming style methods like
`Array#map` and `Array#reduce`, which take a function as an argument.

Say we have an array of numbers, that we need to double the value of, then add
all the doubled numbers together to find their total.

This sounds like a good time to map and reduce.


```javascript
var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
var doubledNumbers = numbers.map(function (number) {
  return number * 2
})
var total = doubledNumbers.reduce(function (total, number) {
  return total + number
}, 0)
total // 110
```

It works, but it's a bit verbose, lots of ugly syntax.

Arrow functions let us omit a lot of that syntax in certain circumstances.

We can omit the return keyword, as long as the function body is only a single
expression, which will be returned implicitly.

```javascript
var numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
var doubledNumbers = numbers.map((number) => number * 2)
var total = doubledNumbers.reduce((total, number) => total + number, 0)
total // 110
```

#### All arrow functions all the time then, yeah???

Arrow functions aren't "simply better" than traditional JS functions.

In this example, we *want* the traditional behaviour, where `this` is set based
on where the function is called.

```javascript
var element = getElementById('thing')
var makeRed = function () {
  this.style.color = '#FF0000'
}
element.onclick = makeRed
```

`makeRed` is not an arrow function and hasn't has `.bind()` called on it.
Because of this, `this.style.color` will refer to whichever element the function
is attached to.
