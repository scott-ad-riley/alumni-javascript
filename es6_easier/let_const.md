# let/const

## WHY
Useful for:
* reliable block scoping of variables
* _loose_ immutability of variables

## WHAT

### let

Behaves like `var` in 99% of cases. It's main difference is that it is no longer _lexically_(function) scoped, it is _block_ scoped.

```javascript
  function doSomething () {
    var apple = "apple"
    {
      var apple = "pear"
    }
    // apple is pear
    // note that even in strict mode, javascript will let us do this
  }
  // with let
  function doSomething() {
    let apple = "apple"
    {
      let apple = "pear" // outside the block, this would throw
      // here, apple's value is "pear"
    }
    // here apple's value is still "apple"
  }

    // in a loop
    for (var i = 0; i < 10; i++) {
      // ...
    }
    // we still have i in scope out here

    for (let i = 0; i < 10, i++) {
      // ...
    }
    // i is no longer in scope
  }
```


### const

same behaviour as above with _block_ scoping. `const` values are designed to provide some level of immutability.

```javascript
  const four = 4
  four = 3 // throws an error
  const name = "Scott"
  name = "Scott2" // throws
  // remember: strings in javascript are immutable
```

```javascript
  const person = {
    name : "Scott"
  }
  person.name = "Craig"
  person.firstName = "Jarrod"
  // no complaints here, the person object we created with the object literal is being mutated
  // `const` purely stores a reference to the object, in this example it's not much use really
```


Final note: use `const` by default unless you need the variable to be immutable (because immutability is good)

It won't protect you from people mutating your object. It will only prevent re-assignment of the the value inside the variable (using it for functions is useful, since it can protect you from mutation there)
