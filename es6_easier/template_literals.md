# Template Literals

## WHY
Useful for:
* string interpolation

## WHAT

Use backticks and then it works like ruby with a hash and curly braces

```javascript
  const name = 'Scott'
  const greeting = `Hello! ${name}, nice to meet you!`
```

You can have more complex expressions in side the `${}` brackets but it's a really bad idea.
