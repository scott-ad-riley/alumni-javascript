// These are the notes we created during the meetup

/********** ARROW FUNCTIONS *************/

// this is not what you think it is inside your setInterval callback

var Timer = function () {
  this.minutes = 0
  setInterval(function () {
    this.minutes++
  }, 60000)
}

// result:
that_func() // equivalent to window.that_func()
window.minutes = NaN

// use arrow function to implicitly bind "this"

var Timer = function () {
  this.minutes = 0
  setInterval(() => {
    this.minutes++
  }, 60000)
}

// reduce the amount of syntax/make more readable

var numbers = [1, 2, 3, 4, 5]

var doubledNumbers = numbers.map(function (number) {
  return number * 2
})

var total = doubledNumbers.reduce(function (total, number ) {
  return total + number
}, 0)

var doubledNumbers = numbers.map(number => number * 2)
                            .reduce((total, number) => (total + number), 0)

// you may not always need arrow functions (when you don't want to auto-bind "this")

var element = document.getElementById('some_id')
var makeRed = function () {
  this.style.color = "#FF0000"
}

element.onclick = makeRed

/********** TEMPLATE LITERALS *************/

const name = "Scott"
const greeting = `Hi there, ${name}!`

// multiple lines will give you everything including whitespace and "\n" (new lines)
const url = `https://api.github.com
    ?user=${user}
    &limit=${limit}`

/********** DEFAULT ARGUMENTS *************/

var Bear = function (name, age, weight) {
  this.name = name
  if (age === undefined) {
    this.age = 0
  } else {
    this.age = age
  }
  this.weight = weight || 0.0
}

var babyBear = new Bear('Yogi')
babyBear.age // 0
babyBear.weight // 0.0

var Bear = function (name, age = 0, weight = 0.0) {
  this.name = name
  this.age = age
  this.weight = weight
}
