# ES6 Modules (Import/Export)

## WHY
Useful for:
* using code from other places
* lets you tree-shake (`require`s/CommonJS dont)
* note the current state of this spec (basically, you need babel)
  * the spec is still being decided upon: https://whatwg.github.io/loader/

## WHAT

### Default export

```javascript
  export default function (name) {
    return name.toUpperCase().split()
  }

  // stuff you can do
  // export default () => {}
  // export default "apple"

  import upcaseAndSplit from './that_file'
  upcaseAndSplit('value') // ["V", "A", "L", "U", "E"]
```

### Named export

```javascript
  export const apple = function (type) {
    return `Oh look! a ${type} apple!`
  }

  import { apple } from './that_file'

  apple("Granny Smith")
  // note this will also protect through with `const` (or atleast babel protects you)
  apple = "something else" // will throw
```

### Abusing the default export

```javascript
  export default {
    one,
    two,
    three
  }

  import { one, two } from './that_file'
```

### Combination of both

```javascript
  export default 1
  export const two = 2
  export const three = 3
  export const four = 4

  import one, {two, three, four} from './that_file'
```



### Static Imports/Tree Shaking

Basically, don't `export default {one, two, three}`, export one thing at a time.
This is so that bundlers can tree-shake your code to remove anything you export that is never used (including other libraries, lodash for example).
Unfortunately, Babel currently transpiles `import`/`export` code to the equivalent `require`
If you export a giant object of things, and another module imports them then webpack/the bundler can't determine which parts are used and which are not.
