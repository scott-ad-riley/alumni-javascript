// These are the notes we created during the meetup

/********** REST + SPREAD *************/

const restArguments = function (name) {
  const myArray = Array.prototype.slice.call(arguments) // ["Craig", 1, 2, 3, 4, 5, 6, 7, 8]
  name // "Craig"
  const numbers = myArray.slice(1) // [1, 2, 3, 4, 5, 6, 7, 8]
}

restArguments("Craig", 1, 2, 3, 4, 5, 6, 7, 8)

// with rest:
const restArguments = function (name, ...numbers) {
  name // "Craig"
  numbers // [1, 2, 3, 4, 5, 6, 7, 8]
}

restArguments("Craig", 1, 2, 3, 4, 5, 6, 7, 8)

const spreadArray = function (a, b, c, d, e, f, g) {
  console.log(a) // 1
  console.log(f) // undefined
}

const myNumbers = [1, 2, 3, 4, 5]

spreadArray(...myNumbers) // passing in an array of arguments

const first = [1, 2, 3]
const second = [4, 5, 6]
const together = [ ...first, ...second ]

const immutableArray = [1, 2, null, 4, 5]

const firstBit = immutableArray.slice(0, 2)
const lastBit = immutableArray.slice(3)

const goodArray = [
  ...immutableArray.slice(0, 2),
  3,
  ...immutableArray.slice(3)
]

const sameOldGoodArray = [...goodArray]



const someObject = {name: "Craig", age: 24, friends: ["Scott", "Jarrod"] }

// part of ES6
const newObject = Object.assign(
  {},
  someObject,
  { age: 25 },
  {friends: someObject.friends.slice()}
)

// requires a flag (in rest_spread.md) to use
const newObject = { ...someObject, age: 25 }

// {name: "Craig", age: 25}

// doesn't work (doesn't know how to iterate over a plain object)
const thing = [...someObject]

/********** DESTRUCTURING *************/

const someBigObject = {
  email: {
    domain: {
      locale: "en"
    }
  },
  username: '@scott_riley'
}

// in another file/somewhere else

const email = someBigObject.email.domain.locale
const username = someBigObject.username

// with destructuring:

const {
  email: {
    domain: {
      locale: locale
    }
  },
  username
} = someBigObject

locale // "en"
username // "@scott_riley"

const array = [1, 2, 3, 4, 5, 6, 7, 8]

const [first, second, ...theRest] = array

first // 1
second // 2
theRest // [3, 4, 5, 6, 7, 8]


myFunc({
  email: 'scott@',
  username: 'scott_username',
  gender: 'M'
})

const myFunc = ({email, username, title="Dr"}) => {
  email // "scott@"
  username // 'scott_username'
  title // 'M' (would be 'Dr' if wasn't passed)
}

// previous way to default export
module.exports = function () {

}

require('./file.js') // get that function out

// using ES6 default function
export default function (name) {
  return name.toUppercase().split()
}

// in another

import upCaseAndSplit from './that_file'

// you can still import with CommonJS/require
var upCaseAndSplit = require('./that_file')

// named exports
export const apple = function (type) {
  return `Oh look! a ${type} apple!`
}

// importing out the named with destructuring
import { apple } from './that_file'

// you can alias it when it comes in
import { apple as importedApple } from './that_file'

// Static Exporting -> Good
export const apple = function () {}
export const pear = function () {}

// Dynamic Exporting -> Bad (because bundler cannot tree shake)
export default {
  apple, pear
}

import { apple } from './that_file'

/********** CLASSES *************/

const Person = function (name) {
  this.name = name
}

Person.prototype = {
  nameWithTitle: function () {
    return "Mr" + this.name
  }
}

// exactly the same thing using a class:

class Person {
  constructor(name) {
    this.name = name
  }

  nameWithTitle() {
    return "Mr" + this.name
  }
}

// Also easy to use inheritance

class Worker extends Person {
  constructor(name, tool) {
    super(name)
    this.tool = tool
  }

  work() {
    this.name
  }
}

new Person("Scott")
