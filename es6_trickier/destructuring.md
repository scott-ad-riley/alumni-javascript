# Destructuring

## WHY
Useful for:
* multiple getting values out of nested structures
* using "options" arguments for functions
  * allows you to whitelist arguments

## WHAT

### Basic

One basic rule:
* The object you are destructuring from (usually on the right side of the equals) *must* be the same type (array or object) as the object on the left.

```javascript
  const { email, user_name, title } = some_big_user_object
  const [ icon_large, icon_medium, icon_small ] = some_icons
  // variables are now in scope
```


### Next Steps
```javascript
  // Destructuring with spread
  const { email, user_name, title, ...user_params } = some_big_object
  const [ icon_large, icon_medium, icon_small, ...other_icons ] = some_icons

  // Destructuring in function arguments
  // 1. whitelisting arguments
  // 2. sort of circumvents any issues with argument order whilst keeping the same behaviour
  const my_func = ({email, user_name, title}) => {
    // ...
  }

  // You can still use defaults too
  const my_func = ({email, user_name, title="Dr"}) => {
    //...
  }

  // Swapping values without needing a weird temp variable
  let x = "should be y"
  let y = "should be x"; // note this semi colon
  [x, y] = [y, x] // note there is no `let` because we'd be declaring x and y twice
```

## HOW (what babel does)

For the basics, it's doing exactly what you would expect...

```javascript
  const {one, two} = three
  const [three, four] = five
  // becomes... (roughly)
  var one = three.one
  var two = three.two
  var three = five[0]
  var four = five[1]
```

Using spread(`...`):

```javascript
  const [one, two, ...three] = four
  // becomes... (roughly)
  var one = four[0]
  var two = four[1]
  var three = four.slice(2)
```

Defaults:

```javascript
  const {one, two, three="bear"} = four
  // becomes... (roughly)
  var one = four.one
  var two = four.two
  // four.three may or may not be undefined here
  var _four$three = four.three
  // if it is use bear, if not, put the result in to three
  var three = _four$three === undefined ? "bear" : _four$three;
```

```javascript
  let x = "should be y"
  let y = "should be x";
  [x, y] = [y, x] // note there is no `let` because we'd be declaring x and y twice
  // becomes
  var x = "should be y";
  var y = "should be x";

  var _ref = [y, x];
  x = _ref[0];
  y = _ref[1];
  _ref;
```
