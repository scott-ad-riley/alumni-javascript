# Rest / Spread operator

## WHY
Useful for:
* Collecting a flexible number of arguments to a function.
* Extracting parts of arrays or objects.

## WHAT

The rest operator can be used to allow us to gather up parameters to a function

Before:

```javascript
var restArguments = function (name) {
  var array = Array.prototype.slice.call(arguments)
  array = array.slice(1)
  return array
}
console.log(restArguments("craig", 1, 2, 3, 4, 5))
// [1, 2, 3, 4 5]
```

With rest operator:

```javascript
var restArguments = function (name, ...array) {
  return array
}
console.log(restArguments("craig", 1, 2, 3, 4, 5))
// [1, 2, 3, 4 5]
```


## Spread

The spread operator can take an array and spread out the elements into individual
arguments to a function.

```javascript
var spreadArray = function (a, b, c, d, e, f, g) {
  console.log(a) // 1
  console.log(f) // undefined
}
var array = [1, 2, 3, 4, 5]
spreadArray(...array)
```

You can spread arrays out into new array literals, allowing you to mix arrays and
individual elements in array literals. This essentially replaces `[].concat`

```javascript
var firstArray = [1, 2, 3]
var secondArray = [4, 5, 6]
var fullArray = [...firstArray, ...secondArray]
```

This is really helpful in redux or any other situation where you need to treat
an array as immutable.

```javascript
var immutableArray = [1, 2, null, 4, 5]
var replaceMiddle = function (array) {
  var firstPart = array.slice(0, 2)
  var lastPart = array.slice(3)
  var fixedArray = [...firstPart, 3, ...lastPart]
  return fixedArray
}
console.log(replaceMiddle(immutableArray))
// [1, 2, 3, 4, 5]
```

The original array is unmodified, and we get back a new array.

## Babel shit

If we look back at that first rest example.

```javascript
var restArguments = function (name, ...whatever) {
  return whatever
}
console.log(restArguments("craig", 1, 2, 3, 4, 5))
// [ "hi", "there", 1, 2, 3 ]
```

Babel transpiles it to this:

```javascript
"use strict";
var restArguments = function restArguments(name) {
  // this is using "1" because, when transpiling, babel saw that we had one argument that wasn't part of the ...rest
  var whatever = Array(_len > 1 ? _len - 1 : 0)
  var _key = 1
  for (var _len = arguments.length; _key < _len; _key++) {
    whatever[_key - 1] = arguments[_key];
  }
  return whatever;
};
console.log(restArguments("craig", 1, 2, 3, 4, 5));
// [ "hi", "there", 1, 2, 3 ]
```


#### Cool dynamic slice using this.length inside the function to get the arity.

We have to use .call to set the `this` value. So now this.length tells us how
many parameters we were expecting. And we can slice them off and just get the array of the optional arguments.

```javascript
var restArguments = function (name) {
  var array = Array.prototype.slice.call(arguments)
  array = array.slice(this.length)
  return array
}
restArguments.call(restArguments, "craig", 1, 2, 3, 4, 5)
// [ "hi", "there", 1, 2, 3 ]
```

Cool, but a bit of a mess, thankfully the ...rest operator handles it.

## Object version

There is also a version of these same operators for objects.

Object spread:
```javascript
var someObject = {name: "Craig", age: 24}
var newObject = {...someObject, age: 25}
```

someObject is not modified, and we get a new object.

Note: You have to spread **before** the values you want to replace.

If you spread after the new `{age: 25}`, you will just replace the new part
with the original from `someObject`. They are applied to the new object from left to right.

This replaces the ugly `Object.assign`:

```javascript
var someObject = {name: "Craig", age: 24}
var newObject = Object.assign({}, someObject, {age: 25})
```

If using these operators for objects, you may need to use a babel plugin:
`plugins: ["transform-object-rest-spread"]`
