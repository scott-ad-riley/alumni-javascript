# Classes

## WHY
Useful for:
* getting syntactic Object Orientation in javascript (note: still just sugar)

## WHAT

```javascript
class Person {
  constructor(name) {
    this.name = name
  }

  nameWithTitle() {
    return "Mr " + this.name
  }
}

class Worker extends Person {
  constructor(name, tool) {
    super(name)
    this.tool = tool
  }

  work() {
    console.log(`Working hard with my ${tool}!`)
  }
}
```

Don't dwell on this too much - just explain that it uses prototypes so we possibly some of the caveats.
