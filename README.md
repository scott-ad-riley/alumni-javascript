



## ES6 topics:

### ES6 easier bits (arrows, let/const, template literals, default args)
19 votes

### ES6 trickier bits (classes, import/export, destructuring, rest/spread)
27 votes

### ES6 tough bits (promises, generators)
(add async / await?)
26 votes